package dod.hackathon.combatfeeding;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AboutNutritionActivity extends Activity implements OnClickListener {

	private LinearLayout parent;
	private TextView mre;
	private TextView aero;
	private TextView prot;
	private TextView carb;
	private TextView nutr;
	
	private TextView mreD;
	private TextView aeroD;
	private TextView protD;
	private TextView carbD;
	private TextView nutrD;
	
	private LinearLayout mreLay;
	private LinearLayout aeroLay;
	private LinearLayout protLay;
	private LinearLayout carbLay;
	private LinearLayout nutrLay;
	
	private boolean fullDetail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_nutrition);
		
		parent = (LinearLayout) findViewById(R.id.parent_nutrition);
		
		mre = (TextView) findViewById(R.id.mre_description);
		aero = (TextView) findViewById(R.id.aerobic_description);
		prot = (TextView) findViewById(R.id.proteins_description);
		carb = (TextView) findViewById(R.id.carbohydrates_description);
		nutr = (TextView) findViewById(R.id.nutrition_overview_description);
		
		mre.setOnClickListener(this);
		aero.setOnClickListener(this);
		prot.setOnClickListener(this);
		carb.setOnClickListener(this);
		nutr.setOnClickListener(this);
		
		mreD = (TextView) findViewById(R.id.mre_full_detail);
		aeroD = (TextView) findViewById(R.id.aerobic_full_detail);
		protD = (TextView) findViewById(R.id.proteins_full_detail);
		carbD = (TextView) findViewById(R.id.carbohydrates_full_detail);
		nutrD = (TextView) findViewById(R.id.nutrition_overview_full_detail);
		
		mreLay = (LinearLayout) findViewById(R.id.mre);
		aeroLay = (LinearLayout) findViewById(R.id.aerobic);
		protLay = (LinearLayout) findViewById(R.id.proteins);
		carbLay = (LinearLayout) findViewById(R.id.carbohydrates);
		nutrLay = (LinearLayout) findViewById(R.id.nutrition_overview);
	}

	@Override
	public void onClick(View v) {
		hideAllViews();
		fullDetail = true;
		
		switch (v.getId()) {
		case R.id.mre_description:
			mre.setVisibility(View.VISIBLE);
			mreD.setVisibility(View.VISIBLE);
			mreLay.setVisibility(View.VISIBLE);
			break;
		case R.id.aerobic_description:
			aero.setVisibility(View.VISIBLE);
			aeroD.setVisibility(View.VISIBLE);
			aeroLay.setVisibility(View.VISIBLE);
			break;
		case R.id.proteins_description:
			prot.setVisibility(View.VISIBLE);
			protD.setVisibility(View.VISIBLE);
			protLay.setVisibility(View.VISIBLE);
			break;
		case R.id.carbohydrates_description:
			carb.setVisibility(View.VISIBLE);
			carbD.setVisibility(View.VISIBLE);
			carbLay.setVisibility(View.VISIBLE);
			break;
		case R.id.nutrition_overview_description:
			nutr.setVisibility(View.VISIBLE);
			nutrD.setVisibility(View.VISIBLE);
			nutrLay.setVisibility(View.VISIBLE);
			break;		
		
		}
		
	}
	
	void hideAllViews() {
		for (int i = 0; i < parent.getChildCount(); i++) {
			View v = parent.getChildAt(i);
			v.setVisibility(View.GONE);
		}
	}
	
	void showAllViews() {
		for (int i = 0; i < parent.getChildCount(); i++) {
			View v = parent.getChildAt(i);
			v.setVisibility(View.VISIBLE);
		}
	}
	
	
	@Override
	public void onBackPressed() {	
		
		if (!fullDetail) {
			super.onBackPressed();
		} else {
			resetViews();
			fullDetail = false;
		}
		
	}

	private void resetViews() {
		showAllViews();
		mreD.setVisibility(View.GONE);
		aeroD.setVisibility(View.GONE);
		nutrD.setVisibility(View.GONE);
		protD.setVisibility(View.GONE);
		carbD.setVisibility(View.GONE);

	}

}
