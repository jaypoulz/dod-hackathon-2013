package dod.hackathon.combatfeeding;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kinvey.android.AsyncAppData;
import com.kinvey.android.callback.KinveyListCallback;

import dod.hackathon.combatfeeding.objects.Gear;
import dod.hackathon.combatfeeding.objects.GearAdapter;

public class GearPicker extends Activity {

	private ListView lv;
	private GearAdapter gearAdapter;
	private ArrayList<Gear> gear;
	public static TextView textTotalWeight;
	private Button ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.picker_gear);

		new FetchFromKinveyTask().execute();
		
		lv = (ListView) findViewById(R.id.picker_gear_listview);
		
		textTotalWeight = (TextView) findViewById(R.id.textTotalWeight);
		
		ok = (Button) findViewById(R.id.picker_gear_ok);
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent iRet = new Intent();
				iRet.putExtra("gear_weight", gearAdapter.totalWeight);
				setResult(RESULT_OK, iRet);
				finish();
			}
		});
		
	}

	private class FetchFromKinveyTask extends AsyncTask<Void, Integer, Void> {

		ProgressDialog dia;
		
		@Override
		protected void onPreExecute() {

			dia = new ProgressDialog(GearPicker.this);
			dia.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dia.setMessage("Please wait while fetching from the database");
			dia.setCancelable(false);
			dia.show();

		}

		@Override
		protected Void doInBackground(Void... voids) {
			
			gear = new ArrayList<Gear>();

			AsyncAppData<Gear> myevents = MainActivity.mKinveyClient.appData(
					"GearWeight", Gear.class);
			myevents.get(new KinveyListCallback<Gear>() {
				@Override
				public void onSuccess(Gear[] results) {
					gear = new ArrayList<Gear>(Arrays.asList(results));
				}

				@Override
				public void onFailure(Throwable error) {
					Log.e("tag", "failed to fetch all", error);
				}
			});
			
			while(gear.size() == 0) {
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			publishProgress(100);
			return null;

		}

		@Override
		protected void onPostExecute(Void voids) {
			dia.setMessage("Done");
			dia.cancel();

			gearAdapter = new GearAdapter(GearPicker.this,
					R.layout.list_element_gear, gear);
			
			lv.setAdapter(gearAdapter);
			lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		}

	}

}
