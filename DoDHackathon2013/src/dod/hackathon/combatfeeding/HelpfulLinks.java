package dod.hackathon.combatfeeding;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HelpfulLinks extends Activity {
	private Button link1;
	private Button link2;
	private Button link3;
	private Button link4;

	private Button ok;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_helpful_links);

		ok = (Button) findViewById(R.id.dialog_links_ok);
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		link1 = (Button) findViewById(R.id.link1);
		link1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://www.nutrition.gov/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		
		link2 = (Button) findViewById(R.id.link2);
		link2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://www.eatright.org/Public/content.aspx?id=7056";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		
		link3 = (Button) findViewById(R.id.link3);
		link3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://www.hss.edu/conditions_burning-calories-with-exercise-calculatingestimated-energy-expenditure.asp";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		
		link4 = (Button) findViewById(R.id.link4);
		link4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "http://armypubs.army.mil/epubs/pdf/r40_25.pdf";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});

	}
}
