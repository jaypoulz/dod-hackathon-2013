package dod.hackathon.combatfeeding.objects;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import dod.hackathon.combatfeeding.GearPicker;
import dod.hackathon.combatfeeding.R;

public class GearAdapter extends ArrayAdapter<Gear> {

	private ArrayList<Gear> gearList;
	private Context context;
	private int layoutId;
	public float totalWeight = 0.0f;

	public enum SortType {
		ITEM_NAME, ITEM_MENU
	};

	public GearAdapter(Context context, int textViewResourceId,
			ArrayList<Gear> original) {
		super(context, textViewResourceId);

		gearList = original;
		this.context = context;
		layoutId = textViewResourceId;
	}

	// For this helper method, return based on filteredData
	public int getCount() {
		return gearList.size();
	}

	// This should return a data object, not an int
	public Gear getItem(int position) {
		return gearList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View view = convertView;
		if (convertView == null) {
			view = inflater.inflate(layoutId, parent, false);
		}

		final CheckedTextView name = (CheckedTextView) view
				.findViewById(R.id.label_name);
		name.setText("" + gearList.get(position).get("WEAPON"));
		TextView mClass = (TextView) view
				.findViewById(R.id.label_class);
		mClass.setText("" + gearList.get(position).get("CLASS"));
		final TextView weight = (TextView) view
				.findViewById(R.id.label_weight);
		weight.setText(gearList.get(position).get("WEAPON LOADED (LBS)") +" lbs");
		
		LinearLayout ll = (LinearLayout) view.findViewById(R.id.list_element_gear_layout);
		ll.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				name.toggle();
				String[] split = weight.getText().toString().split(" ");
				if (name.isChecked()) {
					totalWeight += Float.parseFloat(split[0]);
					GearPicker.textTotalWeight.setText("Total Weight: " + totalWeight);
				} else {
					totalWeight -= Float.parseFloat(split[0]);
					GearPicker.textTotalWeight.setText("Total Weight: " + totalWeight);
				}
			}	
		});

		return view;

	}

}
